﻿using System.Collections;
using UnityEngine;

namespace SimpleGame
{
    public class ObjectManager : MonoBehaviour
    {
        private Coroutine _spawnCoroutine;
        private ObjectCreator _objectCreator;

        private void Awake()
        {
            _objectCreator = gameObject.AddComponent<BallCreator>();
            EventManager.Subscribe<GameStartEvent>(OnGameStart);
            EventManager.Subscribe<GameOverEvent>(OnGameOver);
        }

        private void OnGameStart(GameStartEvent eventData)
        {
            _spawnCoroutine = StartCoroutine(Spawn());
        }

        private void OnGameOver(GameOverEvent eventData)
        {
            if (_spawnCoroutine != null)
                StopCoroutine(_spawnCoroutine);
        }

        private IEnumerator Spawn()
        {
            while (true)
            {
                _objectCreator.Create();
                yield return new WaitForSeconds(StaticData.ObjectSpawnTime);
            }
        }

        private void OnDestroy()
        {
            EventManager.Unsubscribe<GameStartEvent>(OnGameStart);
            EventManager.Unsubscribe<GameOverEvent>(OnGameOver);
        }
    }
}