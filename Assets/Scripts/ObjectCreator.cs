﻿using System;
using UnityEngine;

namespace SimpleGame
{
    public abstract class ObjectCreator : MonoBehaviour
    {
        public abstract BaseObject Create();

        protected virtual BaseObject InstantiateObject(string path)
        {
            var randomColor = GetRandomColor();
            path += randomColor;

            var prefab = Resources.Load<GameObject>(path);
            if (prefab == null)
                throw new Exception("Prefab not found: Resources/" + path);

            var go = Instantiate(prefab);
            var randomMultiplier = GetRandomMultiplier();
            var randomPosition = GetRandomPosition(randomMultiplier);
            var obj = go.GetComponent<BaseObject>();

            go.transform.position = randomPosition;
            obj.Initialize(randomMultiplier);
            return obj;
        }


        private ObjectColors GetRandomColor()
        {
            var values = Enum.GetValues(typeof(ObjectColors));
            var random = new System.Random();
            return (ObjectColors)values.GetValue(random.Next(values.Length));
        }

        private float GetRandomMultiplier()
        {
            return UnityEngine.Random.Range(0.5f, 1.5f);
        }

        private Vector3 GetRandomPosition(float multiplier)
        {
            var objectSize = StaticData.ObjectDefaultSize * multiplier;
            var leftBottomPoint = Camera.main.ScreenToWorldPoint(Vector3.zero);
            var rightBottomPoint = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0));

            var minX = leftBottomPoint.x + objectSize / 2;
            var maxX = rightBottomPoint.x - objectSize / 2;

            var randomX = UnityEngine.Random.Range(minX, maxX);
            var y = leftBottomPoint.y - objectSize;

            var position = new Vector3(randomX, y);
            return position;
        }
    }
}