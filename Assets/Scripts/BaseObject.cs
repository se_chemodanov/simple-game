﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace SimpleGame
{
    public class BaseObject : MonoBehaviour, IPointerClickHandler
    {
        private float _speed, _scores;
        private Transform _transform;

        // The larger the object, the smaller the scores and the speed
        public void Initialize(float multiplier)
        {
            _scores = StaticData.ObjectDefaultScore / multiplier;
            _speed = StaticData.ObjectDefaultSpeed / multiplier;
            _transform.localScale *= multiplier;
            Destroy(gameObject, StaticData.ObjectDeathTimeDelay);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            EventManager.Publish(new KillObjectEvent(_scores));
            Destroy(gameObject);
        }


        private void Awake()
        {
            _transform = transform;
        }

        private void Update()
        {
            _transform.Translate(Vector3.up * _speed * Time.deltaTime);
        }
    }
}