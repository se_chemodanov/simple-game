﻿using UnityEngine;

namespace SimpleGame
{
    public class GameController : MonoBehaviour
    {
        private float _scores;
        private float _time;

        private void Awake()
        {
            EventManager.Subscribe<PlayerInputRestartEvent>(OnPlayerClickRestart);
            EventManager.Subscribe<KillObjectEvent>(OnKillObject);
        }

        private void Start()
        {
            _time = StaticData.LevelTime;
            EventManager.Publish(new GameStartEvent());
        }

        private void Update()
        {
            if (_time <= 0)
                return;

            var newTime = _time -= Time.deltaTime;
            ChangeTime(newTime);
        }

        private void OnPlayerClickRestart(PlayerInputRestartEvent eventData)
        {
            ChangeScores(0);
            ChangeTime(StaticData.LevelTime);
            EventManager.Publish(new GameStartEvent());
        }

        private void OnKillObject(KillObjectEvent eventData)
        {
            AddScores(eventData.Scores);
        }

        private void AddScores(float amount)
        {
            var newScores = _scores + amount;
            ChangeScores(newScores);
        }

        private void ChangeScores(float newValue)
        {
            _scores = newValue;
            EventManager.Publish(new ScoresChangeEvent(_scores));
        }

        private void ChangeTime(float newValue)
        {
            _time = newValue;
            EventManager.Publish(new TimeChangeEvent(_time));

            if (_time > 0)
                return;

            _time = 0;
            EventManager.Publish(new GameOverEvent(_scores));
        }

        private void OnDestroy()
        {
            EventManager.Unsubscribe<PlayerInputRestartEvent>(OnPlayerClickRestart);
            EventManager.Unsubscribe<KillObjectEvent>(OnKillObject);
        }
    }
}
