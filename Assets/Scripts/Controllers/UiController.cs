﻿using UnityEngine;
using UnityEngine.UI;

namespace SimpleGame
{
    public class UiController : MonoBehaviour
    {
        [SerializeField] private Text _scores, _time, _result;
        [SerializeField] private GameObject _gameOverWindow;
        [SerializeField] private Button _restartButton;

        private void Awake()
        {
            _gameOverWindow.SetActive(false);
            _restartButton.onClick.AddListener(OnPlayerClickRestart);
            EventManager.Subscribe<ScoresChangeEvent>(OnScoresChange);
            EventManager.Subscribe<TimeChangeEvent>(OnTimeChange);
            EventManager.Subscribe<GameOverEvent>(OnGameOver);
            EventManager.Subscribe<GameStartEvent>(OnGameStart);
        }

        private void OnPlayerClickRestart()
        {
            EventManager.Publish(new PlayerInputRestartEvent());
        }

        private void OnTimeChange(TimeChangeEvent eventData)
        {
            _time.text = eventData.Time.ToString("F");
        }

        private void OnScoresChange(ScoresChangeEvent eventData)
        {
            _scores.text = eventData.Scores.ToString("######");
        }

        private void OnGameStart(GameStartEvent eventData)
        {
            _scores.text = "0";
            _gameOverWindow.SetActive(false);
        }

        private void OnGameOver(GameOverEvent eventData)
        {
            _scores.text = "0";
            _time.text = "0";
            _result.text = string.Format("Your result: {0:#}", eventData.Scores);
            _gameOverWindow.SetActive(true);
        }

        private void OnDestroy()
        {
            _restartButton.onClick.RemoveListener(OnPlayerClickRestart);
            EventManager.Unsubscribe<ScoresChangeEvent>(OnScoresChange);
            EventManager.Unsubscribe<TimeChangeEvent>(OnTimeChange);
            EventManager.Unsubscribe<GameOverEvent>(OnGameOver);
            EventManager.Unsubscribe<GameStartEvent>(OnGameStart);
        }
    }
}