﻿namespace SimpleGame
{
    public class StaticData
    {
        public static float LevelTime = 50f;
        public static float ObjectSpawnTime = 0.3f;
        public static float ObjectDefaultSize = 1f;
        public static float ObjectDefaultSpeed = 5f;
        public static float ObjectDefaultScore = 50;
        public static float ObjectDeathTimeDelay = 5f;
    }
}