﻿namespace SimpleGame
{
    public class BallCreator : ObjectCreator
    {
        public override BaseObject Create()
        {
            var path = "Ball";
            var ball = InstantiateObject(path);
            return ball;
        }
    }
}