﻿namespace SimpleGame
{
    public class TimeChangeEvent
    {
        public float Time { get; private set; }

        public TimeChangeEvent(float time)
        {
            Time = time;
        }
    }
}