﻿namespace SimpleGame
{
    public class KillObjectEvent
    {
        public float Scores { get; private set; }

        public KillObjectEvent(float scores)
        {
            Scores = scores;
        }
    }
}