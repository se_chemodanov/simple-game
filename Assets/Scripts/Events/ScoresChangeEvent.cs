﻿namespace SimpleGame
{
    public class ScoresChangeEvent
    {
        public float Scores { get; private set; }

        public ScoresChangeEvent(float scores)
        {
            Scores = scores;
        }
    }
}