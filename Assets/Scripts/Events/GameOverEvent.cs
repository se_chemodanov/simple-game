﻿namespace SimpleGame
{
    public class GameOverEvent
    {
        public float Scores { get; private set; }

        public GameOverEvent(float scores)
        {
            Scores = scores;
        }
    }
}